// Jordan Cooke 1217804
// Theo Newton 1145457

import java.io.*;
import java.util.*;
import java.lang.*;

class loadboat
{
	public static void main(String args[]) throws IOException
	{
		List<cargo> allCargo = new ArrayList<cargo>();
		cargo[] bestCargo;
		cargo[] currentCargo;
		String line = "";
		float maxVolume, maxWeight, maxCost;
		float bestProfit = 0;
		float currentProfit = 0;
		int cargoSize, numChanges, maxTime;
		double currentTime, oldTime, endTime, coolingTime, runTime;
		
		if(args.length < 5){
			System.out.println("Error not enough arguments");
		}
		//reads the file and adds it to an array of cargo
		else{
			BufferedReader reader = new BufferedReader(new FileReader(args[4]));
			maxVolume = Float.parseFloat(args[0]);
			maxWeight = Float.parseFloat(args[1]);
			maxCost = Float.parseFloat(args[2]);
			maxTime = Integer.parseInt(args[3]);
			
			while((line = reader.readLine()) != null){
				String[] parts = line.split(" ");
				
				float volume = Float.parseFloat(parts[0]);
				float weight = Float.parseFloat(parts[1]);
				float cost = Float.parseFloat(parts[2]);
				float value = Float.parseFloat(parts[3]);
				
				cargo tempCargo = new cargo(volume, weight, cost, value);
				allCargo.add(tempCargo);
			}
			
			//sets up the cargo arrays and random number generator, and sets the run time and end time to current nano time and current nano time + how long it is to run for
			//run time is used to print the total time the program ran for and end time is used to stop the program when it reaches it
			cargoSize = allCargo.size();
			bestCargo = new cargo[cargoSize];
			currentCargo = new cargo[cargoSize];
			Random randomGenerator = new Random();
			runTime = System.nanoTime();
			endTime = System.nanoTime() + (maxTime * 1000000000.0);
			
			//Keeps finding random cargos until it finds one that fits into the max volume, weight and cost
			//If it cant find  one a valid one in the giving max time it reports no cargo
			while(bestProfit == 0){
				int tempInt = randomGenerator.nextInt(cargoSize);
				for(int i = 0; i < tempInt; i++){
					int r = randomGenerator.nextInt(cargoSize);
					bestCargo[r] = allCargo.get(r);
				}
				bestProfit = calculateCost(bestCargo, maxVolume, maxWeight, maxCost);
				if(bestProfit == 0){
					Arrays.fill(bestCargo, null);
				}
				if(System.nanoTime() > endTime) break;
			}
			
			//sets the cooling rate  and cargo
			currentCargo = bestCargo.clone();
			numChanges = cargoSize / 2;
			coolingTime = (maxTime * 1000000000.0) / numChanges;
			oldTime = System.nanoTime();
			
			//Loops until it runs out of  time,  numChanges is the number of changes each loop, which is reduced when the currentTime reaches previous time + coolingTime
			//coolingTime is the total run time divided by the initial numChanges, so it will always end on 1 change for the last loops.
			//it will check the profit each loop, if it is higher then previous max profit it will swap them, otherwise it will move on
			while(System.nanoTime() < endTime){
				for(int m = 0; m < numChanges; m++){
					int r = randomGenerator.nextInt(cargoSize);
					if(currentCargo[r] == null){
						currentCargo[r] = allCargo.get(r);
					}
					else currentCargo[r] = null;
				}
				currentProfit = calculateCost(currentCargo, maxVolume, maxWeight, maxCost);
				if(currentProfit > bestProfit){
					bestProfit = currentProfit;
					bestCargo = currentCargo.clone();
				}
				else{
					currentCargo = bestCargo.clone();
					currentProfit = bestProfit;
				}
				//every time  amount of  time has gone past, reduce the numChanges by one
				if((currentTime = System.nanoTime()) >= oldTime + coolingTime){
					oldTime = currentTime;
					numChanges--;
				}
			}
			
			//puts the index of each used cargo into a string to print
			String finalCargo = "";
			for(int i = 0; i < cargoSize; i++){
				if(bestCargo[i] != null){
					if(finalCargo == ""){
						finalCargo = "" + i;
					}
					else{
						finalCargo = finalCargo + "," + i;
					}
				}
			}
			
			System.out.println(finalCargo);
			
		}
	}
	
	//calculates the profit of a given set of cargo and checks to see if it is a valid selection of cargo, if it isnt the profit is returned as 0
	public static float calculateCost(cargo[] cargoArray, float maxVolume, float maxWeight, float maxCost){
		float totalVolume = 0; 
		float totalWeight = 0;
		float totalCost = 0;
		float totalValue = 0;
		
		for(cargo c : cargoArray){
			if(c != null){
				totalVolume = totalVolume + c.volume;
				totalWeight = totalWeight + c.weight;
				totalCost = totalCost + c.cost;
				totalValue = totalValue + c.value;
			}
		}
		if(totalVolume < maxVolume && totalWeight < maxWeight && totalCost < maxCost){
			return totalValue - totalCost;
		}
		else{
			return 0;
		}
	}
}
